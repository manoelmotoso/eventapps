﻿$(window).resize(function () {
    var winwidth = $(window).innerWidth();
    var winheight = $(window).innerHeight();
    $(".header-video video").css({ "min-width": winwidth, "min-height": winheight - 73 });
    $(".carousel-cell").css({ "width": winwidth });
});
$(function () {
    var winwidth = $(window).innerWidth();
    var winheight = $(window).innerHeight();
    $(".header-video video").css({ "min-width": winwidth, "min-height": winheight - 73 });
    $(".carousel-cell").css({ "width": winwidth });

});

$(document).on("scroll", function () {
    var scroll = $(this).scrollTop();
    if (scroll > 0) {
        $("nav").removeClass("affix-top");
        $("nav").addClass("affix");
    } else {
        $("nav").addClass("affix-top");
        $("nav").removeClass("affix");
    }
});
$(".nav-wrap").load("./header.html");
$("footer").load("./footer.html");